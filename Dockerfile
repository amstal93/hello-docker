FROM debian:stable-slim
RUN apt-get update && apt-get install -yqq build-essential
COPY hello.c Makefile /
RUN make

FROM scratch
COPY --from=0 /hello .
CMD ["/hello"]
